ENROLL TPM WITH PANTAHUB ACA
============================

This is a initial approach to test the enrollment protocol on pantahub ACA



```bash
./enroll.sh folder
```

Will create an ek.pub and ak, send it to the aca server who responde with the credential, that will be activated

### TODO

The server needs to return a certificate encrypted using the credential

The client need to decrypt the certificate using the credential and stored in the TPM