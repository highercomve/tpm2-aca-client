#!/bin/bash

if [ -z "$1" ]; then
  TMP_FOLDER=$(mktemp -d /tmp/enroll.XXXXXX)
else
  TMP_FOLDER=$1
fi

ACA_URL=http://localhost:12368
CERTIFICATE_FILE="$TMP_FOLDER/certificate.in"
CREDENTIAL_FILE="$TMP_FOLDER/credential.in"
SESSION_FILE="$TMP_FOLDER/session"
ACTIVATION_OUTPUT="$TMP_FOLDER/credential.out"
EK_FILE="$TMP_FOLDER/ek.pub"
EK_HANDLE=0x81010001
AK_HANDLE=0x81010002
EK_LOADED="EK loaded from $EK_HANDLE"
EK_CREATED="New EK created on $EK_HANDLE"
AK_CTX="$TMP_FOLDER/ak.ctx"
AK_PUB="$TMP_FOLDER/ak.pub"
AK_NAME="$TMP_FOLDER/ak.name"
AK_OUT="$TMP_FOLDER/ak.out"

mkdir $TMP_FOLDER

tpm2_readpublic -c $EK_HANDLE -o $EK_FILE
if [ $? -eq 0 ]; then echo $EK_LOADED; else echo $EK_CREATED; tpm2_createek -Q -c $EK_HANDLE -G rsa -u $EK_FILE; fi

tpm2_createak -C $EK_HANDLE -c $AK_CTX -G rsa -g sha256 -s rsassa -u $AK_PUB -n $AK_NAME > $AK_OUT

# if [ -f "$AK_CTX" ]; then
#   echo "Ak context exist persisting on TPM"
#   tpm2_evictcontrol -c $AK_CTX $AK_HANDLE
# else
#   echo "Ak context does not exist, need to be created"
#   tpm2_createak -C $EK_HANDLE -c $AK_CTX -G rsa -g sha256 -s rsassa -u $AK_PUB -n $AK_NAME > $AK_OUT
#   tpm2_evictcontrol -c $AK_CTX $AK_HANDLE
# fi

FILE_NAME=$(cat $AK_NAME | xxd -p -c $(stat --printf="%s" $AK_NAME))
EK_CERT=$(cat $EK_FILE | base64 -w 0)

response=$(curl --request POST --url $ACA_URL/enroll-request/ --header 'content-type: application/json' --data "{ \"ak-pub\": \"YQo=\", \"ak-name\": \"${FILE_NAME}\", \"ek-cert\": \"${EK_CERT}\"}")

echo $response | jq '.certificate' -r | base64 -d > $CERTIFICATE_FILE
echo $response | jq '.credential' -r | base64 -d > $CREDENTIAL_FILE

tpm2_startauthsession --policy-session -S $SESSION_FILE
tpm2_policysecret -S $SESSION_FILE -c e
tpm2_activatecredential -Q -c $AK_CTX -C $EK_HANDLE -i $CREDENTIAL_FILE -o $ACTIVATION_OUTPUT -P"session:$SESSION_FILE"
tpm2_flushcontext $SESSION_FILE

echo "Secret: "
cat $ACTIVATION_OUTPUT
echo ""
echo "All keys are storage on: $TMP_FOLDER"